# OpenML dataset: medical_charges

https://www.openml.org/d/41444

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The Inpatient Utilization and Payment Public Use File (Inpatient PUF) provides information on inpatient discharges for Medicare fee-for-service beneficiaries. The Inpatient PUF includes information on utilization, payment (total payment and Medicare payment), and hospital-specific charges for the more than 3,000 U.S. hospitals that receive Medicare Inpatient Prospective Payment System (IPPS) payments. The PUF is organized by hospital and Medicare Severity Diagnosis Related Group (MS-DRG) and covers Fiscal Year (FY) 2011 through FY 2016. For this version, some features were removed and all remaining character features were recoded as nominal factor variables.
                      The variable 'Average.Medicare.Payments' is used as target by default.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41444) of an [OpenML dataset](https://www.openml.org/d/41444). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41444/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41444/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41444/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

